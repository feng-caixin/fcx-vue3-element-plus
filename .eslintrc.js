module.exports = {
  root: true,
  env: {
    node: true
  },
  extends: [
    'plugin:vue/vue3-essential',
    '@vue/standard'
  ],
  parserOptions: {
    parser: 'babel-eslint'
  },
  rules: {
    'no-console': process.env.NODE_ENV === 'production' ? 'warn' : 'off',
    'no-debugger': process.env.NODE_ENV === 'production' ? 'warn' : 'off',
    'space-before-function-paren': 0,
    'quotes':'off',
    'semi':'off',
    'comma-dangle':'off',
    'no-multiple-empty-lines' :0,
    'space-before-blocks' :0,
    'indent': ["off", 2],
    'eqeqeq':'off',
    'quote-props':'off',
    'no-unused-vars':'off',
    'no-trailing-spaces':'off',
    'vue/no-mutating-props':'off'
  }
}
