// 菜单相关的接口

import { allTree } from '@/data/menu'

/**
 * 获取所有菜单
 * @returns promise
 */
export const getAllTree = () => {
  return new Promise(function(resolve, reject){
     // 当异步代码执行成功时，我们才会调用resolve(...), 当异步代码失败时就会调用reject(...)
    // 在本例中，我们使用setTimeout(...)来模拟异步代码，实际编码时可能是XHR请求或是HTML5的一些API方法.
    setTimeout(function(){
      resolve({ code: 0, data: allTree, extra: null, msg: "ok" }) // 代码正常执行！
    }, 500)
  })
}


