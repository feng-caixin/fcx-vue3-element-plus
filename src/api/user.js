// 用户相关的接口

import { guid } from '@/utils/index'
import { userList } from '@/data/user'

/**
 * 帐号密码登录
 * @param {String} account - 帐号
 * @param {String} password - 密码
 * @returns promise
 */
export const userAccountLogin = ({ account, password }) => {
  return new Promise(function(resolve, reject){
    // 当异步代码执行成功时，我们才会调用resolve(...), 当异步代码失败时就会调用reject(...)
    // 在本例中，我们使用setTimeout(...)来模拟异步代码，实际编码时可能是XHR请求或是HTML5的一些API方法.
    setTimeout(function(){
        if (account == '1001' && password == '123456') {
          resolve({ code: 0, data: { user: { id: "1", account: "1001", token: guid(), name: "平台管理员" } }, extra: null, msg: "ok" }) // 代码正常执行！
        } else {
          reject(new Error({ code: -1, data: null, extra: null, msg: "验证码不正确" }))
        }
    }, 500)
  })
}

/**
 * 获取用户列表
 * @returns promise
 */
export const getList = () => {
  return new Promise(function(resolve, reject){
     // 当异步代码执行成功时，我们才会调用resolve(...), 当异步代码失败时就会调用reject(...)
    // 在本例中，我们使用setTimeout(...)来模拟异步代码，实际编码时可能是XHR请求或是HTML5的一些API方法.
    setTimeout(function(){
      resolve({ code: 0, data: userList, extra: null, msg: "ok" }) // 代码正常执行！
    }, 500)
  })
}



