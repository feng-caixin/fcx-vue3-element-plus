export const orgTree = [
    {
        "id": "001",
        "createTime": "2019-07-25 15:35:12",
        "createUser": "1",
        "updateTime": "2021-07-10 10:27:50",
        "updateUser": "1",
        "name": "fcx责任有限公司",
        "describe": "fcx责任有限公司",
        "level": 1,
        "isEnable": 1,
        "sortValue": 4,
        "parentId": "0",
        "address": null,
        "abbreviation": null,
        "children": [
          {
            "id": "001001",
            "createTime": "2019-07-25 15:35:12",
            "createUser": "1",
            "updateTime": "2021-07-10 10:27:50",
            "updateUser": "1",
            "name": "信息部",
            "describe": "",
            "level": 2,
            "isEnable": 1,
            "sortValue": 4,
            "parentId": "001",
            "address": null,
            "abbreviation": null
          }, {
            "id": "001002",
            "createTime": "2019-07-25 15:35:12",
            "createUser": "1",
            "updateTime": "2021-07-10 10:27:50",
            "updateUser": "1",
            "name": "设备部",
            "describe": "",
            "level": 2,
            "isEnable": 1,
            "sortValue": 4,
            "parentId": "001",
            "address": null,
            "abbreviation": null
          }, {
            "id": "001003",
            "createTime": "2019-07-25 15:35:12",
            "createUser": "1",
            "updateTime": "2021-07-10 10:27:50",
            "updateUser": "1",
            "name": "财务部",
            "describe": "",
            "level": 2,
            "isEnable": 1,
            "sortValue": 4,
            "parentId": "001",
            "address": null,
            "abbreviation": null
          }, {
            "id": "001004",
            "createTime": "2019-07-25 15:35:12",
            "createUser": "1",
            "updateTime": "2021-07-10 10:27:50",
            "updateUser": "1",
            "name": "技术部",
            "describe": "",
            "level": 2,
            "isEnable": 1,
            "sortValue": 4,
            "parentId": "001",
            "address": null,
            "abbreviation": null
          }, {
            "id": "001005",
            "createTime": "2019-07-25 15:35:12",
            "createUser": "1",
            "updateTime": "2021-07-10 10:27:50",
            "updateUser": "1",
            "name": "商贸部",
            "describe": "",
            "level": 2,
            "isEnable": 0,
            "sortValue": 4,
            "parentId": "001",
            "address": null,
            "abbreviation": null
          }]
    }
]

