export const userRouters = [
  {
      "path": "/system",
      "name": "system",
      "component": "layout/index",
      "meta": {
          "title": "系统管理",
          "icon": "Setting"
      },
      "hidden": false,
      "children": [
          {
              "path": "org/manage",
              "name": "org_manage",
              "component": "system/org/manage/index",
              "meta": {
                  "title": "组织管理",
                  "icon": ""
              },
              "hidden": false
          },
          {
              "path": "user/manage",
              "name": "user_manage",
              "component": "system/user/manage/index",
              "meta": {
                  "title": "用户管理",
                  "icon": ""
              },
              "hidden": false
          },
          {
              "path": "auth/menu",
              "name": "auth_menu",
              "component": "system/auth/menu/index",
              "meta": {
                  "title": "菜单配置",
                  "icon": ""
              },
              "hidden": false
          }
      ]
  }
]

export const resourceList = [
    {
        "id": "643444685339100195",
        "createTime": "2019-11-11 13:39:28",
        "createUser": "3",
        "updateTime": "2019-11-11 13:39:50",
        "updateUser": "3",
        "code": "resource:add",
        "name": "添加",
        "method": "POST",
        "url": "/resource",
        "menuId": "603976297063910529",
        "describe": ""
    },
    {
        "id": "643444685339100196",
        "createTime": "2019-11-11 13:39:28",
        "createUser": "3",
        "updateTime": "2019-11-11 13:39:50",
        "updateUser": "3",
        "code": "resource:update",
        "name": "修改",
        "method": "PUT",
        "url": "/resource",
        "menuId": "603976297063910529",
        "describe": ""
    },
    {
        "id": "643444685339100197",
        "createTime": "2019-11-11 13:39:28",
        "createUser": "3",
        "updateTime": "2019-11-11 13:39:50",
        "updateUser": "3",
        "code": "resource:delete",
        "name": "删除",
        "method": "DELETE",
        "url": "/resource",
        "menuId": "603976297063910529",
        "describe": ""
    },
    {
        "id": "643445641149680705",
        "createTime": "2019-11-11 13:43:16",
        "createUser": "3",
        "updateTime": "2019-11-11 13:43:16",
        "updateUser": "3",
        "code": "menu:add",
        "name": "添加",
        "method": "POST",
        "url": "/menu",
        "menuId": "603976297063910529",
        "describe": ""
    },
    {
        "id": "643445674330819745",
        "createTime": "2019-11-11 13:43:24",
        "createUser": "3",
        "updateTime": "2019-11-11 13:43:24",
        "updateUser": "3",
        "code": "menu:update",
        "name": "修改",
        "method": "PUT",
        "url": "/menu",
        "menuId": "603976297063910529",
        "describe": ""
    },
    {
        "id": "643445704177487105",
        "createTime": "2019-11-11 13:43:31",
        "createUser": "3",
        "updateTime": "2019-11-11 13:43:31",
        "updateUser": "3",
        "code": "menu:delete",
        "name": "删除",
        "method": "DELETE",
        "url": "/menu",
        "menuId": "603976297063910529",
        "describe": ""
    },
    {
        "id": "643445747320098145",
        "createTime": "2019-11-11 13:43:41",
        "createUser": "3",
        "updateTime": "2020-03-03 19:55:10",
        "updateUser": "3",
        "code": "menu:view",
        "name": "查看",
        "method": "GET",
        "url": "/menu/tree",
        "menuId": "603976297063910529",
        "describe": ""
    },
    {
        "id": "643445774687931841",
        "createTime": "2019-11-11 13:43:48",
        "createUser": "3",
        "updateTime": "2019-11-11 13:43:48",
        "updateUser": "3",
        "code": "menu:export",
        "name": "导出",
        "method": "GET",
        "url": "/menu",
        "menuId": "603976297063910529",
        "describe": ""
    },
    {
        "id": "643445802106097185",
        "createTime": "2019-11-11 13:43:54",
        "createUser": "3",
        "updateTime": "2019-11-11 13:43:54",
        "updateUser": "3",
        "code": "menu:import",
        "name": "导入",
        "method": "POST",
        "url": "/menu",
        "menuId": "603976297063910529",
        "describe": ""
    },
    {
        "id": "684539815017848257",
        "createTime": "2020-03-03 23:16:50",
        "createUser": "3",
        "updateTime": "2020-03-03 23:16:50",
        "updateUser": "3",
        "code": "resource:view",
        "name": "查看",
        "method": "GET",
        "url": "/resource/page",
        "menuId": "603976297063910529",
        "describe": ""
    }
]


  
