// 权限拦截在路由跳转  导航守卫

import router from '@/router'
import store from '@/store' // 引入store实例 和组件中的this.$store是一回事
import nprogress from 'nprogress' // 引入进度条
import 'nprogress/nprogress.css' // 引入进度条样式
import { getUserRouters } from '@/api/permission'

let asyncRouter

const whiteList = ['/login'] // 定义白名单
router.beforeEach(async(to, from, next) => {
  nprogress.start() // 开启进度条的意思

  if (whiteList.indexOf(to.path) > -1) {
    // 表示在白名单里面
    next()
  } else {
  const { profile } = store.state.user
    if (!asyncRouter){
      //  如果有token
      if (profile.token) {
        asyncRouter = await store.dispatch('permission/getUserRouters')
        // console.log(asyncRouter)
        routerPackag(asyncRouter, null)
        next(to.path)
      } else { 
        next('/login')
      }
    } else {
      next()
    }
  }
})
// 后置守卫
router.afterEach(() => {
  nprogress.done() // 关闭进度条
})
// 动态加载路由
const routerPackag = (routers, pRouter) => {
  routers.forEach(itemRouter => {
    console.log('@/views/' + itemRouter.component)
    if (pRouter == null) {
      router.addRoute({
        path: itemRouter.path,
        name: itemRouter.name,
        meta: itemRouter.meta,
        hidden: itemRouter.hidden,
        component: () => import('@/views/' + itemRouter.component)
      })
    } else {
      router.addRoute(pRouter.name, {
        path: itemRouter.path,
        name: itemRouter.name,
        meta: itemRouter.meta,
        hidden: itemRouter.hidden,
        component: () => import('@/views/' + itemRouter.component)
      })
    }
    // 是否存在子集
    if (itemRouter.children && itemRouter.children.length) {
      routerPackag(itemRouter.children, itemRouter)
    }
    return true
  })
}

