// 专门处理权限路由的模块
import { constantRoutes } from '@/router'
import { getUserRouters } from '@/api/permission'

const mutations = {
  // newRoutes认为是用户登录之后获取的新路由
  setRoutes(state, newRoutes) {
    state.routes = [...constantRoutes, ...newRoutes] // 静态路由  + 动态路由
    // 需要得到newRoutes 才能调用mutations
  }
}
const actions = {
   // 获取用户路由信息
  async getUserRouters (ctx) {
    // 准备合并的参数
    const { data } = await getUserRouters()
    ctx.commit('setRoutes', data)
    return data
  }
}

const getters = {
  // 用户菜单
  userMenus (state) {
    return state.routes.filter(items => items.hidden == false)
  }
}

export default {
  namespaced: true,
  state () {
    return {
       // 一开始 肯定拥有静态路由的权限
       routes: constantRoutes // 路由表  表示 当前用户所拥有的所有路由的数组
    }
  },
  mutations,
  actions,
  getters
}
