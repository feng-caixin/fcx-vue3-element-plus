import { createStore } from 'vuex'
import createPersistedState from 'vuex-persistedstate'

// 模块
import user from './modules/user'
import permission from './modules/permission'

export default createStore({
  modules: {
    user,
    permission
  },
  // 配置插件
  plugins: [
    // 默认存储在localStorage
    createPersistedState({
      // 本地存储名字
      key: 'brickdog-client-pc-store',
      // 指定需要存储的模块
      paths: ['user']
    })
  ]
})
