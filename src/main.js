import { createApp } from 'vue'

import ElementPlus from 'element-plus'
import 'element-plus/dist/index.css'
import './styles/index.scss' // global css
import App from './App.vue'
import router from './router'
import store from './store'
import * as ElIconModules from '@element-plus/icons-vue'
import '@/permission' // permission control
import locale from 'element-plus/lib/locale/lang/zh-cn' // 中文


// 创建一个vue应用实例
var app = createApp(App)
.use(store)
.use(router)
.use(ElementPlus, { locale })
Object.keys(ElIconModules).forEach(function(key) {
  app.component(ElIconModules[key].name, ElIconModules[key])
})

app.mount('#app')
