module.exports = {
  css: {
      loaderOptions: {
          sass: {
              prependData: `
              @import "@/styles/mixin.scss";
              @import "@/styles/variables.scss";
              `
          }
      }
  }
}

